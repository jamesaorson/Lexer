package Compiler.Scanner;

import java.io.IOException;
import static Compiler.Scanner.TokenType.*;

/**
  * Performs two runs of the CMinusScanner to test its lexing.
  *
  * @author James Osborne and Jeremy Tiberg
  * File: ScannerTest.java
  * Created:  29 Jan 2018
  * Description: This test simply lexes a C- source with and without errors
  * and outputs the results to separate files.
  **/

public class ScannerTest {
    private static CMinusScanner scanner;
    private static Token token;
            
    public static void main(String[] args) throws IOException {
        scanner = new CMinusScanner("test0.c-");
        
        for (Integer i = 0; i <= 4; ++i) {
            scanner.resetScanner("test" + i.toString() + ".c-");
            token = scanner.viewNextToken();
            
            while (token.getTokenType() != ERROR_TOKEN &&
                   token.getTokenType() != EOF_TOKEN) {
            token = scanner.getNextToken();
        }

        scanner.writeToOutputFile("test" + i.toString() + ".lex");
        }
    }
}
